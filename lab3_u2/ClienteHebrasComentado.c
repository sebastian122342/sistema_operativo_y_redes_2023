/*
Sebastián Bustamante
Se comentan las funciones del código
Se comenta la idea y objetivo general del programa
Se comentan ideas posibles de por qué el programa no funciona
*/

// Librerias a utilizar
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>

////
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>

// Puerto y tamaño del buffer
#define PORT 8000
#define BUFFERSIZE 1024

// Crea el socket
void crearSocket(int *sock){
	// Se entrega dominio, tipo de conexion y protocolo
	// Si no se puede crear, se notifica y acaba el programa
    if ((*sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        printf("Error Creación de Socket\n");
        exit(0);
    }
}

// Configura el cliente
void configurarCliente(int sock, struct sockaddr_in *conf){
	conf->sin_family = AF_INET; // Domino
	conf->sin_port = htons(PORT); // Puerto a conectarse
	conf->sin_addr.s_addr = inet_addr("127.0.0.1"); // ip localhost

	// Conecta y si falla se notifica y acaba el programa
	if (connect(sock, (struct sockaddr *)conf, sizeof(*conf)) < 0){
		printf("\nConnection Failed \n");
		exit(0);
	}
}

int main(int argc, char const *argv[]){
	/*
	La idea general del codigo es enviar y recibir mensajes
	del servidor.
	Esto funciona de manera ciclica hasta que el mensaje del cliente sea
	BYE, y en ese caso se acaba la conexion y el programa.
	Para el primer mensaje, el servidor contesta de manera diferente que
	para los demás.

	El codigo funciona bien, es el servidor quien presenta problemas al
	momento de manejar a los clientes.

	*/
    if (argc < 2)
        return 0;

	// Recibir el nombre del cliente
    char nombreClientes[100];
    strcpy(nombreClientes, argv[1]);

	///1. Crear Scoket
    int sockCliente;
    crearSocket(&sockCliente);

    ///2. Conectarse al Servior
    struct sockaddr_in confServidor;
    configurarCliente(sockCliente, &confServidor);

    int primerMensaje = 1;
    ///3. Comunicarse
	while(1){
		// Buffers para manejar textos
		char buffer[BUFFERSIZE]={0}, buffer2[BUFFERSIZE]={0};

		// Para el primer mensaje se ejecuta esta seccion
		if (primerMensaje){
		   // Se envia el nombre del clinte al servidor
    	   send(sockCliente, nombreClientes, strlen(nombreClientes), 0);
		   primerMensaje = 0; // De ahora en adelante se ejecutará la otra seccion

		   // Se recibe la respuesta del buffer
    	   int valread = read(sockCliente, buffer, BUFFERSIZE);
		   fputs(buffer, stdout);
		// Del segundo el adelante se ejecuta esta seccion
		}else{
			  // Se escribe un mensaje para el server y se conserva en el buffer
 			  printf("Mensaje: ");
			  fgets(buffer, BUFFERSIZE, stdin);

			  // Si el mensaje es BYE se acaba la conexion
			  if (strcmp(buffer, "BYE\n")==0){
				  // Enviar el mensaje al server
				  send(sockCliente, buffer, strlen(buffer), 0);
				  // Recibe la respuesta del server
				  int valread = read(sockCliente, buffer2, BUFFERSIZE);
				  strcat(buffer2, nombreClientes);
   			      fputs(buffer, stdout);
				  break;
			  // Si el mensaje no es bye, se envia el mensaje y se espera respuesta
			  }else{
				  // Enviar mensaje
				  send(sockCliente, buffer, strlen(buffer), 0);
				  // Esperar respesta
				  int valread = read(sockCliente, buffer, BUFFERSIZE);
				  // Mostrar respuesta
				  printf("%s",buffer);
			  }
		}
	}

	// Cerrar el Socket
	close(sockCliente);

	return 0;
}
