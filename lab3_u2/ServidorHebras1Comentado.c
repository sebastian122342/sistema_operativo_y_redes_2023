/*
Sebastián Bustamante
Se comentan las funciones del código
Se comenta la idea y objetivo general del programa
Se comentan ideas posibles de por qué el programa no funciona
*/

// Liberias a utilizar
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

////
#include <netinet/in.h>
#include <sys/socket.h>
#include <arpa/inet.h>

////
#include <pthread.h>

// Puerto y tamaño del buffer
#define port 8000
#define BUFFERSIZE 1024

// Se crea un socket
void crearSocket(int *sock){
	// Usa dominio, tipo de conexion y protocolo
	// Si no se puede crear, se notifica y acaba el programa
    if ((*sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        printf("Error Creación de Socket\n");
        exit(0);
    }
}

// Configura el servidor y se intenta enlazar
void configurarServidor(int socket, struct sockaddr_in *conf){
    conf->sin_family = AF_INET;		   		    // Dominio
    conf->sin_addr.s_addr = htonl(INADDR_ANY);	// Enlazar con cualquier dirección local
    conf->sin_port = htons(port);				// Puerto donde escucha

	// Si no se puede enlazar se notifica y se acaba el programa
    if ((bind(socket, (struct sockaddr *) conf, sizeof(*conf))) < 0) { // bind!
        printf("Error de enlace\n");
        exit(0);
    }
}

// Se escucha a los clientes, si falla se notifica y se acaba al programa
void escucharClientes(int sock, int n){
    if (listen(sock, n) < 0) {
        printf("Error listening\n");
        exit(0);
    }
}

// Se acepta la conexion al Socket, si falla se notifica y acaba el programa
void aceptarConexion(int *sockNuevo, int sock, struct sockaddr_in *conf ){
    int tamannoConf = sizeof(*conf);

    if ((*sockNuevo = accept(sock, (struct sockaddr *) conf, &tamannoConf)) < 0) {
        printf("Error accepting\n");
        exit(0);
    }
}

// Se reemplaza un caracter por otro en un buffer
void reemplazarCaracter(char *buffer, char caracter1, char caracter2){
    for(int j=0; j < strlen(buffer); j++){
        if (buffer[j] == caracter1){
            buffer[j] = caracter2;
        }
    }
}

// Invierte una palabra dada, ejemplo "cosa" -> "asoc"
void invertirPalabra(char *palabra){
    int largo = strlen(palabra);
    char palabra2[largo];

    for (int i = 0; i < largo; i++)  {
        palabra2[i] = palabra[largo - i - 1];
    }

    palabra2[largo] = '\0';
    strcpy(palabra, palabra2);
}

void *Servidor(void *arg){
   // Se recibe el socket
   int *sockCliente = (int *)arg;
   /////
   int primerMensaje = 1; // Para el primer mensaje del servidor
   while(1){
        char nombre[BUFFERSIZE]={0}; // Nombre del cliente
        char buffer[BUFFERSIZE]={0}; // Buffers para manejar texto
        char buffer2[BUFFERSIZE]={0};
        int valread; // Para leer las respuestas del cliente

		// Se recibe el primer mensaje del cliente
        valread = read(*sockCliente, buffer, BUFFERSIZE);

		// Seccion que se ejecuta si es el primer mensaje
        if (primerMensaje){
            strcpy(buffer2, "Hola ");
            strcat(buffer2, buffer);
            strcat(buffer2, "\n");
            strcpy(nombre, buffer);

            printf("%s", buffer2);
            send(*sockCliente, buffer2, strlen(buffer2), 0);
            primerMensaje = 0;
		// Para el segundo mensaje en adelante se ejecuta esta seccion
        }else{
			// Si el mensaje es BYE se responde y se acaba el ciclo
            if (strcmp(buffer, "BYE\n")==0){
                strcpy(buffer2, buffer);
                strcat(buffer2, nombre);
                send(*sockCliente, buffer2, strlen(buffer2), 0);

                printf("%s", buffer2);
                break;
            }

			// Si el mensaje no es BYE, se ejecuta esta seccion
            printf("%s", buffer);

            ////Separar en palabras el mensaje recibido
            char buffer2[1024]={0}, *palabra; // Se redeclara el buffer
            palabra = strtok (buffer, " ");
            int primeraPalabra = 1;

			// Invertir las palabras del mensaje enviado por el cliente
            while (palabra != NULL){
                //Invertir palabra
                if (palabra[strlen(palabra)-1]=='\n'){
                    palabra[strlen(palabra)-1]='\0';
                }

                invertirPalabra(palabra);

                if (primeraPalabra){
                    strcpy(buffer2, palabra);
                    primeraPalabra = 0;
                }else{
                    strcat(buffer2, " ");
                    strcat(buffer2, palabra);
                }

                palabra = strtok (NULL, " ");
            }

			// Se envia el mensaje invertido de vuelta al cliente
            send(*sockCliente, buffer2, strlen(buffer2), 0);
        }
   }
}

int main(int argc, char *argv[]){
	/*
	La idea general del codigo es:
	Ser un servidor que puede recibir una cierta cantidad de clientes
	El servidor puede recibir y enviar mensajes al cliente, siempre
	que el cliente quiera mantener la conexion.
	El servidor recibe el mensaje del cliente y responde con el mismo mensaje,
	pero inverso:
		Por ejemplo, si el mensaje es "hola", el servidor responderá "aloh".

	Actualmente el programa no compila, eliminando el [i] dentro del ciclo
	for de al final de la funcion main empieza a compilar, pero no funciona bien

	Ideas de por qué no funciona bien:
	En la función servidor se debería cerrar el socket al terminar la conexión,
	esto no esta explicitado actualmente en la función.
	Tambien se debería terminar el trabajo de la hebra.

	Para cuando se trabajen con más de un cliente, se debe crear un socket
	para cada uno, pero actualmente se crea un solo socket para todos los clientes.
	Esto generaria un error de enlace una vez el resto del programa funcione
	y se quiera trabajar con más de un cliente.

	En la funcion de Servidor, se vuelve a redeclarar el buffer2, una vez
	esta ya se ha declarado.

	Se deberia pasar el argumento entregado al servidor a int otra vez, en vez
	de seguir trabajandolo como referencia

	Al final de la función principal, se debería esperar a que los trabajos de las
	hebras termine con join()

	Tambien se podria implementar la acumulacion de las hebras en un arreglo
	para su mejor manejo.

	*/
    if (argc < 2)
        return 0;

	// Cantidad de clientes con las que se trabajará
    int nClientes = strtol(argv[1], NULL, 10);

    if (nClientes < 1)

       return 0;

    ///1. Configuración del Socket
    int sockServidor;
    crearSocket(&sockServidor);

    ///2. Vinculación
    struct sockaddr_in confServidor;
    configurarServidor(sockServidor, &confServidor);

    ///3. Escuchando conexiones entrantes
    escucharClientes(sockServidor, nClientes);

    ///4. Aceptar conexión
    struct sockaddr_in confCliente;
    int sockCliente;

	// Se crea una hebra por cliente, usando el mismo socket
    for(int i=0; i < nClientes; i++){
        aceptarConexion(&sockCliente, sockServidor, &confCliente);

        pthread_t hebra;
        pthread_create(&hebra, NULL, Servidor, (void *)&sockCliente[i]);
   }

    return 0;
}
