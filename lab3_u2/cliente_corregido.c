#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>

////
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>

// Se usa el puerto 8000
// Tamaño del buffer 1024
#define PORT 8000
#define BUFFERSIZE 1024

// Crea un socket, si falla, se notifica y termina el programa
// AF_INET es dominio de comunicacion
// SOCK_STREAM  es el tipo de comunicacion, en este caso
// comunicacion en ambos sentidos, confiable
// O es el protocolo
void crearSocket(int *sock){
    if ((*sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        printf("Error Creación de Socket\n");
        exit(0);
    }
}
// Configura el socket del cliente
void configurarCliente(int sock, struct sockaddr_in *conf){
	conf->sin_family = AF_INET; // Direccion de la familia
	conf->sin_port = htons(PORT); // Configura el puerto
	conf->sin_addr.s_addr = inet_addr("127.0.0.1"); // Direccion ip es localhost

	// Si falla la conexión se notifica y termina el programa
	if (connect(sock, (struct sockaddr *)conf, sizeof(*conf)) < 0){
		printf("\nConnection Failed \n");
		exit(0);
	}
}

int main(int argc, char const *argv[]){
    if (argc < 2)
        return 0;

    // El nombre del cliente es el argumento entregado al ejecutar
    char nombreClientes[100];
    strcpy(nombreClientes, argv[1]);

    ///1. Crear Socket
    int sockCliente;
    crearSocket(&sockCliente);

    ///2. Conectarse al Servidor (+ se configura antes de conectar)
    struct sockaddr_in confServidor;
    configurarCliente(sockCliente, &confServidor);

    int primerMensaje = 1;
    ///3. Comunicarse
	while(1){
		// Buffer para tener los mensajes que van y vienen
		// buffer para los mensajes que vienen (se guardan ahi)
		// buffer2 para los mensajes que van (se guardan ahi)
		char buffer[BUFFERSIZE]={0}, buffer2[BUFFERSIZE]={0};

		// Solo para el primer mensaje
		if (primerMensaje){
		  // Envia el nombre del cliente al servidor
		  send(sockCliente, nombreClientes, strlen(nombreClientes), 0);
		  primerMensaje = 0; // Se iguala a 0 pues ya se envio el primer mensaje

			// Lee la respuesta del servidor, la guarda en buffer
    	    int valread = read(sockCliente, buffer, BUFFERSIZE);
		    fputs(buffer, stdout);
		// Para el segundo mensaje en adelante
		}else{
			// Se guarda el mensaje para el server en el buffer
			printf("Mensaje: ");
			fgets(buffer, BUFFERSIZE, stdin);

			// Si el mensaje el BYE\n se acaba la comunicacion y el programa
			if (strcmp(buffer, "BYE\n")==0){
			    // Envia en mensaje
				send(sockCliente, buffer, strlen(buffer), 0);
				// Lee la respuesta del server
				int valread = read(sockCliente, buffer2, BUFFERSIZE);
				strcat(buffer2, nombreClientes);
   			    fputs(buffer, stdout);
				break;
			// Si no, envio normal de mensajes
			}else{
				// Envia el mensaje
				send(sockCliente, buffer, strlen(buffer), 0);
				// Lee la respuesta, se guarda en el buffer
				int valread = read(sockCliente, buffer, BUFFERSIZE);
				printf("%s",buffer);
			}
		}
	}

	// Cerrar el socket antes de terminar
	close(sockCliente);

	return 0;
}
