#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <string.h>

/////////
struct valores{
    int inicio;
    int fin;
};

typedef struct valores valores;
////

void *hebraTrabajadora(void *nombreArg){
    char *nombre = (char*)nombreArg;

    for(int i = 0; i < 1; i++){
        usleep(50000);
        printf("Hola! Soy la Hebra de nombre %s - %d\n", nombre, i);
    }
    printf("Hebra %s lista!\n", nombre);     

    return NULL;
}

void *hebraSumadora(void *direccionTotal){

    valores *inicio_fin = (valores *)direccionTotal;

    printf("Soy Sumadora: %d - %d\n", inicio_fin->inicio, inicio_fin->fin);
    sleep(10);
    return NULL;
}

int main(void){
    pthread_t hebra1, hebra2, hebra3;

    char n1[20], n2[20];
    strcpy(n1, "Hebra1");
    strcpy(n1, "Hebra2");

    pthread_create(&hebra1, NULL, hebraTrabajadora, n1);
    pthread_create(&hebra2, NULL, hebraTrabajadora, n2);

    ///
    valores v;
    v.inicio = 1; v.fin = 10;
    pthread_create(&hebra3, NULL, hebraSumadora, &v);
    ///
    //sleep(10);
    pthread_join(hebra1, NULL);
    pthread_join(hebra2, NULL);
    pthread_join(hebra3, NULL);

    printf("Fin del programa principal\n");

    return 0;
}