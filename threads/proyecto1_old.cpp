/*
Ejercicio 1 - Proyecto 2 Sistemas operativos y redes
Sebastián Bustamante

Se lleva a cabo un problema productor-consumidor,
los productores depositan sus productos en un bufer mientras que los
consumidores obtienen los productos desde este.

Se utilizan semaforos y mutex para sincronizar todo el trabajo. Se reciben
parametros desde la linea de comandos, verificando que estos sean validos
para el funcionamiento del programa.

Se muestra en pantalla las acciones que se van ejecutando, también se
conservan en archivos txt que funcionan como un log.
*/

// Librerias a utilizar
#include <iostream>
#include <thread>
#include <mutex>
#include <queue>
#include <fstream>
#include <semaphore.h>
#include <string>
#include <bits/stdc++.h>
#include <unistd.h>
#include <cstdlib>
#include <ctime>

using namespace std;

// Variables globales
int np, npp, nc, ncc, bc; // parametros de entrada
queue<string> buffer; // buffer
mutex mtx;
sem_t empty_slots;
sem_t filled_slots; // mutex y semaforos para sincronizar
string log_prod = "";
string log_cons = ""; // Estas variables sirven como log y mas tarde se
                     // conservan en archivos

// Se produce con el formato id_n°producto
string produce(int id, int num_prod){
    string str_id = to_string(id);
    string str_num_prod = to_string(num_prod);
    string product = str_id + "_" + str_num_prod;
    cout << id << " produjo " << product << endl;
    return product;
}

// Funcion para un productor
void producer_func(int num){
    string msg;
    int id = num;
    for (int i=0; i < npp; i++){
        string product = produce(id, i+1);
        sem_wait(&empty_slots);
        mtx.lock(); // prevenir que otros productores se metan al bufer
        if(buffer.size() == bc){
            // buffer lleno, libera lock y espera
            mtx.unlock();
            msg = to_string(id) + " " + product + "Buffer lleno - Error de inserción";
            cout << msg << endl;
            log_prod += msg + "\n";
            // esperar entre 0 a 4 segundos e intentar de nuevo
            sleep(rand() % 5);
            i--;
            continue;
        }
        buffer.push(product);
        msg = to_string(id) + " " + product + " Inserción exitosa";
        cout << msg << endl;
        log_prod += msg + "\n";
        sleep(rand() % 5); // entre 0 y 4 segundos(?)
        mtx.unlock();
        sem_post(&filled_slots);
    }
}

// Funcion para un consumidor
void consumer_func(int num){
    string msg;
    int id = num;
    for(int i=0; i < ncc; i++){
        sem_wait(&filled_slots);
        mtx.lock(); // evitar que se metan otros consumidores
        msg = to_string(id) + " intentando eliminar el elemento del búfer";
        cout << msg << endl;
        log_cons += msg + "\n";
        if(buffer.empty()){
            // buffer vacio, liberar el lock y esperar
            mtx.unlock();
            msg = "buffer vacio, consumidor " + to_string(id) + " espera";
            cout << msg << endl;
            log_cons += msg + "\n";
            sem_post(&filled_slots); // ir liberando el semaforo
            sleep(rand() % 5);
            continue;
        }
        string consumed = buffer.front();
        buffer.pop();
        msg = to_string(id) + " " + consumed + " eliminado con éxito";
        cout << msg << endl;
        log_cons += msg + "\n";
        sleep(rand() % 5); // se duerme de 0 a 4 segundos
        mtx.unlock();
        sem_post(&empty_slots);
    }
}

// Funcion principal
int main(int argc, char *argv[]){

    srand(time(NULL)); // Seed para la generacion de numeros aleatorios

    // Verificar que se ingrese una cantidad valida de datos
    if (argc != 6){
        cout << "Cantidad de datos no valida" << endl;
        return 0;
    }

    // Se guardan los valores en las variables globales
    np = atoi(argv[1]);
    npp = atoi(argv[2]);
    nc = atoi(argv[3]);
    ncc = atoi(argv[4]);
    bc = atoi(argv[5]);

    // Se verifica que los parametros cumplan con las relaciones requeridas
    if (np*npp < nc*ncc || bc == 0){
        cout << "Datos no validos" << endl;
        return 0;
    }

    // Inicializacion de semaforos
    sem_init(&filled_slots, 0, 0);
    sem_init(&empty_slots, 0, bc);

    // Arreglos para almacenar las hebras
    thread consumers[nc];
    thread producers[np];

    // Se crean productores y consumidores
    for (int i=0; i < np; i++)
        producers[i] = thread(producer_func, i);
    for (int i=0; i < nc; i++)
        consumers[i] = thread(consumer_func, i);

    // Se espera a que terminen productores y consumidores
    for (int i=0; i < np; i++)
        producers[i].join();
    for (int i=0; i < nc; i++)
        consumers[i].join();

    // Destruccion de semaforos
    sem_destroy(&empty_slots);
    sem_destroy(&filled_slots);

    // Notificar termino del programa y guardar el historial
    cout << "Terminando..." << endl;

    // Archivos a usar
    ofstream prod_regist;
    ofstream cons_regist;
    prod_regist.open("productores.txt");
    cons_regist.open("consumidores.txt");

    // Se guarda la informacion
    prod_regist << log_prod;
    cons_regist << log_cons;

    // Se cierran los archivos
    prod_regist.close();
    cons_regist.close();

    return 0;
}
