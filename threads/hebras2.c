#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>

void *hebraTrabajadora(void *nombreArg)
{
    char *nombre = (char*)nombreArg;

    for(int i = 0; i < 100; i++){
        usleep(50000);
        printf("Hola! Soy la Hebra de nombre %s - %d\n", nombre, i);
    }
    printf("Hebra %s lista!\n", nombre);     
    return NULL;
}

int main(void){
    pthread_t hebra1, hebra2;

    pthread_create(&hebra1, NULL, hebraTrabajadora, "Hebra1");
    pthread_create(&hebra2, NULL, hebraTrabajadora, "Hebra2");

    sleep(10);
    printf("Fin del programa principal\n");

    return 0;
}