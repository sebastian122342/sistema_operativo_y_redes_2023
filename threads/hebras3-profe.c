#include <stdio.h>
#include <pthread.h>
#include <semaphore.h>
#include <unistd.h>

sem_t semaforo;

void* FuncionHebra(void* arg){
	//Intentar entrar a la región crítica...
	sem_wait(&semaforo);
    //Revisa si el semáforo es verde! Si es así lo decrementa en 1, y entra...
    //TODA ESTA OPERACIÓN ES ATÓMICA
    
    ///////REGIÓN CRÍTICA!!!!!
	printf("\nAdentro..\n");
	sleep(4);
	printf("\nAdiós!!!!...\n");
    ////

    ///LIBREAR (incrementar en 1 el semáforo)
	sem_post(&semaforo);

    return NULL;
}


int main(){
	///Inicialización 
    sem_init(&semaforo, 0, 1);

	pthread_t hebra1, hebra2;

	pthread_create(&hebra1,NULL,FuncionHebra, NULL);
	pthread_create(&hebra2,NULL,FuncionHebra,NULL);
	
    pthread_join(hebra1,NULL);
	pthread_join(hebra2,NULL);
	
    sem_destroy(&semaforo);

	return 0;
}
