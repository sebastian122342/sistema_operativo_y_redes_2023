# Proyecto 2 de Sistema Operativo y Redes
por Sebastiían Bustamante Villarreal

Se ha desarrollado una herramienta que permite calcular el uso de codones de un organismo, a partir de su archivo de anotación (gbff).
Para que el programa funcione se debe tener descargado este archivo de anotacion dentro del directorio en el cual se va a trabajar,
y en el codigo modificar las variables filename y esp\_name para que se encuentre el archivo y en el grafico se vea correctamente el nombre de la especie.

Tambien se cuenta con una herramienta que basada en las preferencias de usos de codones, puede generar una secuencia nucleotidica óptima, teniendo el codon preferido
para cada aminoacido de la secuencia aminoacidica entregada.
