// Laboratorio 3 - Ejercicio 2 - Sebastián Bustamante

#include <stdio.h>
#include <pthread.h>
#include <semaphore.h>

// Semaforo y variable global
int impresiones = 0;
sem_t semaforo;

// Funcion que se llama al crear cada hebra
void* nace_hebra(void* num){
    int n_hebra = *((int *)num);
	for (int i=0; i < 5; i++){
		// Se entra a la zona critica
		sem_wait(&semaforo);
		impresiones++; // Aumenta el valor de impresiones
		printf("Hilo %d impresiones %d\n", n_hebra, impresiones);
		sem_post(&semaforo); // Se sale de la zona critica
	}
    pthread_exit(NULL);
}

int main(){

    int num_hebras = 5;
    int resp;
    pthread_t hebras[num_hebras];
	int inds[num_hebras];

	// Se inicializa el semaforo
	sem_init(&semaforo, 0, 1);

    // Se crean las 5 hebras
    for (int i=0; i < num_hebras; i++){
		inds[i] = i; // Se guarda el indice de cada hebra para evitar errores
        resp = pthread_create(&hebras[i], NULL, nace_hebra, (void *)&inds[i]);
        if (resp == 0){
            printf("\n");
        }else{
            printf("Error al crear la hebra %d\n", i);
        }
    }

	// Se espera a que acaben las 5 hebras
	for (int i=0; i < num_hebras; i++){
		pthread_join(hebras[i], NULL);
	}

	// Destruir el semaforo
	sem_destroy(&semaforo);

	// Se muestra el valor final
	printf("\nValor final de impresiones: %d\n", impresiones);

    return 0;
}
