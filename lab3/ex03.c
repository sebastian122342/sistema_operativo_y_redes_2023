// Laboratorio 3 - Ejercicio 3 - Sebastián Bustamante

#include <stdio.h>
#include <pthread.h>
#include <semaphore.h>
#include <stdlib.h>
#include <time.h>

// 5 hebras en total, definida como constante, tambien se crea el
#define N 5
sem_t semaforo;

struct info_hilo{
	int suma_hilo; // Suma total de la hebra
	int m; // Hasta donde debe sumar la hebra
	int id_hebra; // id de la hebra
};

typedef struct info_hilo info_hilo;

// Funcion que se llama al crear cada hebra
void* nace_hebra(void* info){
    info_hilo *estructura = (info_hilo *)info;
	sem_wait(&semaforo); // Entra a zona critica
	int suma = 0;
	for (int i=0; i < estructura->m; i++) // Suma de 1 a M
		suma += i;
	sem_post(&semaforo); // Sale de zona critca
	estructura->suma_hilo = suma; // Se guarda la suma

    pthread_exit(NULL);
}

int main(){

    int resp;
    pthread_t hebras[N];
	info_hilo info[N];
	int suma_total = 0;
	srand(time(NULL));

	sem_init(&semaforo, 0, 1);

    // Se crean las 5 hebras
    for (int i=0; i < N; i++){
		info_hilo datos_hebra;
		datos_hebra.m = rand() % 11 + 5; // M sera aleatorio entre 5 y 15
		datos_hebra.id_hebra = i;
		info[i] = datos_hebra;
        resp = pthread_create(&hebras[i], NULL, nace_hebra, (void *)&info[i]);
        if (resp == 0){
            printf("\n");
        }else{
            printf("Error al crear la hebra %d\n", i);
        }
    }

	// Se espera a que acaben las 5 hebras
	for (int i=0; i < N; i++){
		pthread_join(hebras[i], NULL);
	}

	sem_destroy(&semaforo);

	// Se Saca la suma de todo y reporte final
	for (int i=0; i < N; i++){
		printf("La hebra %d sumo de 1 a %d, resultando %d\n",
				info[i].id_hebra, info[i].m, info[i].suma_hilo);
		suma_total += info[i].suma_hilo;
	}

	printf("\nSuma total: %d\n", suma_total);

    return 0;
}
