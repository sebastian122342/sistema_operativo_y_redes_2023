// Laboratorio 3 - Ejercicio 1 - Sebastián Bustamante

#include <stdio.h>
#include <pthread.h>

// Funcion que se llama al crear cada hebra
void* nace_hebra(void* num){
    int n_hebra = *((int *)num);
	// Cada hebra imprime 5 numeros consecutivos, partiendo desde donde
	// termino la anterior, 5*0 para hebra 0, 5*1 para hebra 1 y asi
	// sucesivamente, esto hace que la hebra 0 imprima del 1 al 5, y la hebra
	// 1 del 6 al 10 (y siguiento el patron correspondiente las demas)
	int inicio = 5*n_hebra;
	for (int i=inicio; i < inicio+5; i++){
		printf("Hilo %d impresiones %d\n", n_hebra, i+1);
	}
    pthread_exit(NULL);
}

int main(){

    int num_hebras = 5;
    int resp;
    pthread_t hebras[num_hebras];
    // Se crean las 5 hebras
    for (int i=0; i < num_hebras; i++){
        resp = pthread_create(&hebras[i], NULL, nace_hebra, (void *)&i);
        if (resp == 0){
            printf("Hebra %d creada\n", i);
        }else{
            printf("Error al crear la hebra %d\n", i);
        }

        // Se espera a que cada hebra acabe
        pthread_join(hebras[i], NULL);
    }

    return 0;
}
