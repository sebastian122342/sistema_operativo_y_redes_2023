// Ejercicio 3 - se crean 3 hijos, cada uno crea al otro formando 3 en total

#include <stdio.h>
#include <stdlib.h>
#include<unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/errno.h>

int main(){

    pid_t h1 = fork(); // Programa principal crea a H1
    if (h1 == 0){
        printf("H1 creado!\n"); // H1 notifica su propia creacion
        pid_t h2 = fork(); // H1 crea a H2
        if (h2 == 0){
            printf("H2 creado!\n"); // H2 notifica su propia creacion
            pid_t h3 = fork(); // H2 crea a H3
            if (h3 == 0){
                printf("H3 creado!\n"); // H3 notifica su propia creacion
            }else if (h3 > 0){
                wait(NULL); // Se espera a que termine H3
                printf("H3 terminado\n");
            }
        } else if (h2 > 0) {
            wait(NULL); // Se espera a que termine H2
            printf("H2 terminado\n");
        }
    }else if (h1 > 0){
        wait(NULL); // Se espera a que termine H1
        printf("H1 terminado\n");
    }

    return 0;
}
