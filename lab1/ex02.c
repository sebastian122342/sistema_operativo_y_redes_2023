// Ejercicio 2 - Se crean tres procesos hijos, cada uno notifica cuando se crea

#include <stdio.h>
#include <stdlib.h>
#include<unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/errno.h>

int main(){

    int num_hijos = 3;
    int i; // iterador en el ciclo for
    pid_t hijo[num_hijos]; // arreglo para almacenar los 3 hijos

    // Se crea cada hijo y se agrega al arreglo
    for(i = 0; i < num_hijos; i++){
        hijo[i] = fork(); // se coloca un hijo en cada espacio del arreglo
        if (hijo[i] == 0){ // cada hijo notifica su creacion
            printf("H%d creado!\n", i+1);
            break;
        }
    }

    if (i == num_hijos){ // El padre
        // Se espera a que terminen los hijos
        int estado;
        for(int j=0; j < num_hijos; j++){
           if (waitpid(hijo[j], &estado, 0) == -1){
              printf("Error\n");
              exit(0);
            }
        }
    }

    return 0;
}
