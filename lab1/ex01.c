// Ejercicio 1 - Se crean 8 procesos en total, padre + 7 hijos

#include <stdio.h>
#include <stdlib.h>
#include<unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/errno.h>

int main(){

    int num_hijos = 7;
    int i; // iterador en el ciclo for
    pid_t hijo[num_hijos]; // arreglo para almacenar los 7 hijos

    // Se crea cada hijo y se agrega al arreglo
    for(i = 0; i < num_hijos; i++){
        hijo[i] = fork(); // se coloca un hijo en cada espacio del arreglo
        if (hijo[i] == 0){ // Cada hijo notifica su creación
            printf("Soy el hijo numero %d y mi pid es %d\n", i+1, getpid());
            break;
        }
    }

    if (i == num_hijos){ // El padre
        // Se espera a que terminen los hijos
        int estado;
        for(int j = 0; j < num_hijos; j++){
           if (waitpid(hijo[j], &estado, 0) == -1){
              printf("Error\n");
              exit(0);
            }
        }
		// Finalmente se muestra la id del padre
		printf("El padre tiene pid %d\n", getpid());
    }

    return 0;
}
