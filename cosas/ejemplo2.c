#include<stdio.h>
#include<unistd.h>
#include <sys/types.h>
#include <sys/errno.h>
#include<time.h>

#define N 1000000

int main(){
    int i;
    clock_t inicio = clock(); // cosa de medir el tiempo de ejecucion
    pid_t hijo[5]; // arreglo para almacenar los 5 hijos

    for(i=0; i < 5; i++){
       hijo[i] = fork(); // se coloca un hijo en cada espacio del arreglo

       if (hijo[i] == 0) //Yo soy el hijo!!! (porque el pid es 0)
       {
          int suma = 0;
          for(int i=0; i < N; i++)
             suma += i;

          printf("El valor de la sumas es: %d\n", suma);
          break;
       }
    }

    if (i == 5){  //SOY EL PADRE!!!!
        //Esperar que TODOS los hijos terminen!!!!
        int estado;

        for(int j=0; j < 5; j++){
           if (waitpid(hijo[j], &estado, 0) == -1){
              printf("Errrrrorrrr\n");
              exit(0);
            }
        }

        clock_t final = clock();
        double tiempo = (double)(final - inicio) / CLOCKS_PER_SEC;

        printf("Tiempo Transcurrido %f\n", tiempo);
    }
}
