// Ejercicio 2 Lab 2 - Sebastián Bustamante

#include <stdio.h>
#include <pthread.h>

// Funcion que se llama al crear cada hebra
void* nace_hebra(void* num){
    int n_hebra = *((int *)num);
    printf("H%d creada!\n\n", n_hebra+1);
    pthread_exit(NULL);
}

int main(){

    int num_hebras = 3;
    int resp;
    pthread_t hebras[num_hebras];
    // Se crean las 3 hebras
    for (int i=0; i < num_hebras; i++){
        resp = pthread_create(&hebras[i], NULL, nace_hebra, (void *)&i);
        if (resp == 0){
            printf("Se creo la hebra %d\n", i+1);
        }else{
            printf("Error al crear la hebra %d\n", i+1);
        }

        // Se espera a que cada hebra acabe
        pthread_join(hebras[i], NULL);
    }

    return 0;
}
