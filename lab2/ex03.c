// Ejercicio 3 Lab 2 - Sebastián Bustamante

#include <stdio.h>
#include <pthread.h>

// Hebras maximas
#define hebras_max 3

// Funcion que se ejecuta cuando se crea una hebra
void* funcion_hebra(void* arg){
    int actual = *((int*) arg);
	actual += 1;
    pthread_t hija = actual;
    // La hebra solo crea otra si aun no se llega al tope
    if (actual <= hebras_max){
        int resp;
        resp = pthread_create(&hija, NULL, funcion_hebra, (void *)&actual);
        if (resp == 0){
            printf("H%d creado\n", actual);
        }else{
            printf("Error al crear la hebra %d\n", actual);
        }
        printf("H%d termina\n", actual);
        // Se espera a que la hebra acabe
        pthread_join(hija, NULL);
    }
}

int main(){
    int resp;
    pthread_t hebra_madre;
	int actual = 1; // Para llevar el conteo de cuantas hebras se han creado

    // Se crea la primera hebra
    resp = pthread_create(&hebra_madre, NULL, funcion_hebra, (void *)&actual);
    if (resp == 0){
        printf("H%d creado\n", actual);
    }else{
        printf("Error al crear la hebra %d\n", actual);
    }
    printf("H%d termina\n", actual);
    // Acaba la hebra madre
    pthread_join(hebra_madre, NULL);
    return 0;
}
