#include <unistd.h>
#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>

////
#include <arpa/inet.h>

#define port 9000

int main(){
    printf("Servidor!!!\n");
    ///1. Configuración del Socket
    int sockServidor;
    if ((sockServidor = socket(AF_INET, SOCK_STREAM, 0)) < 0) {        
        printf("Error Creación de Socket\n");
        exit(0);
    }

    printf("Canal de Comunicación Creado\n");

    ///2. Vinculación
    struct sockaddr_in confServidor;

    confServidor.sin_family = AF_INET;		   		    // Dominio
    confServidor.sin_addr.s_addr = htonl(INADDR_ANY);	// Enlazar con cualquier dirección local
    confServidor.sin_port = htons(port);				// Puerto donde escucha

    if ((bind(sockServidor, (struct sockaddr *) &confServidor, sizeof(confServidor))) < 0) { // bind!
        printf("Error de enlace\n");
        exit(0);
    }

    printf("Canal de Comunicación Enlazado\n");

    ///3. Escuchando conexiones entrantes
    if (listen(sockServidor, 5) < 0) {		
        printf("Error listening\n");
        exit(0);
    }

    printf("Canal de Comunicación Escuchando\n");

    ///4. Aceptar conexión
    int sockCliente;
    struct sockaddr_in confCliente;
    int tamannoCliente = sizeof(confCliente);

   
    if ((sockCliente = accept(sockServidor, (struct sockaddr *) &confCliente, &tamannoCliente)) < 0) {
        printf("Error accepting\n");
        exit(0);
    }

   ///Crear una hebra o proceso para Atender al Cliente!!!!

    char ipCliente[256]={0};
    inet_ntop(AF_INET, &confCliente.sin_addr, ipCliente, 256);
    printf("Canal de Comunicación Acepta Comunicación - Cliente %s\n", ipCliente);

    ///// Comunicaciones
    while(1){
        char buffer[1024]={0};
        char *hola = "Saludos desde Servidor";
        int valread;
        
        valread = read(sockCliente, buffer, 1024);
        printf("Servidor: %s\n", buffer);
        
     //   send(sockCliente, hola, strlen(hola), 0);
     //   printf("Servidor: mensaje enviado\n");

        if (strcmp(buffer, "Chao")==0){
            break;
        }
    }

    return 0;
}