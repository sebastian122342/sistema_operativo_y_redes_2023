#include <stdio.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <string.h>

#define PORT 9000

int main(int argc, char const *argv[]){
	///1. Crear Scoket
    int sockCliente;

	if ((sockCliente = socket(AF_INET, SOCK_STREAM, 0)) < 0){
		printf("\n Socket creation error \n");
		return -1;
	}

    ///2. Conectarse al Servior
    struct sockaddr_in confServidor;

	confServidor.sin_family = AF_INET;
	confServidor.sin_port = htons(PORT);

	// Dirección del servidor
	if (inet_pton(AF_INET, "127.0.0.1", &confServidor.sin_addr) <= 0){
		printf("\nInvalid address/ Address not supported \n");
		return -1;
	}

	if (connect(sockCliente, (struct sockaddr *)&confServidor, sizeof(confServidor)) < 0){
		printf("\nConnection Failed \n");
		return -1;
	}

    ///3. Comunicarse
	while(1){
		char hola[1000];
		printf("Mensaje: ");
		scanf("%s", hola);

		char buffer[1024]={0};
		int valread;

		send(sockCliente, hola, strlen(hola), 0);
		printf("Cliente: mensaje enviado\n");
		
///		valread = read(sockCliente, buffer, 1024);
///		printf("Cliente: %s\n",buffer );

		if (strcmp(hola, "Chao")==0)
		   break;
	}

	return 0;

}
