# Ejercicio 1 - Proyecto 1 Sistemas operativos y redes
Por Sebastián Bustamante

Se lleva a cabo un problema productor-consumidor,
los productores depositan sus productos en un bufer mientras que los
consumidores obtienen los productos de este.

Algunas consideraciones:

* Los parametros recibidos por la linea de comandos representan,
respectivamente, la cantidad de productores, la cantidad de consumidores,
la capacidad del buffer, la cantidad de productos a producir por cada productor
y la cantidad de productos a consumir por cada consumidor.

* Se asume que el usuario ingresara solo valores numericos enteros.
El programa valida que los valores ingresados sean positivos y que cumplan
la condicion de que no se vaya a consumir mas de lo que se vaya a producir,
ademas de que la diferencia entre la cantidad a producir y consumir pueda
ser almacenada dentro del buffer.

* Se utilizan semaforos y mutex para sincronizar todo el trabajo.

* Los parametros ingresados son validados para asegurar un buen funcionamiento
del programa.

    * En caso de que haya una cantidad de productos que no sea suficiente para
    abastecer a todos los consumidores, el programa no se ejecuta.
    * Si la cantidad de productos a producir es mayor que la cantidad de
    productos a consumir, y esta diferencia es mayor a la capacidad del buffer,
    el programa tampoco se ejecuta, pues los productos sobrantes no se
    podrian almacenar en su totalidad dentro del buffer.
    * Si algun parametro es negativo o igual a 0, el programa no se ejecuta.
    * Si no se ingresan 5 argumentos, el programa no se ejecuta.
    * Si un productor, al momento de insertar un elemento en el bufer,
    no puede porque este esta lleno, espera hasta que haya un espacio y
    luego de eso lo inserta.
    * Si un consumidor, al momento de consumir un elemento del bufer, no puede
    porque este esta vacio, queda esperando hasta que se agregue algun elemento
     y luego de eso consume.

* Se muestra en pantalla las acciones que se van ejecutando, también se
conservan en archivos txt que funcionan como un log o historial.
