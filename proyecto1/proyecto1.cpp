/*
Ejercicio 1 - Proyecto 1 Sistemas operativos y redes
Sebastián Bustamante

Se lleva a cabo un problema productor-consumidor,
los productores depositan sus productos en un bufer mientras que los
consumidores obtienen los productos desde este.
*/

// Librerias a utilizar
#include <iostream>
#include <thread>
#include <mutex>
#include <queue>
#include <fstream>
#include <semaphore.h>
#include <string>
#include <unistd.h>
#include <cstdlib>
#include <ctime>

using namespace std;

// Variables globales
int np, npp, nc, ncc, bc; // parametros de entrada
queue<string> buffer; // buffer
mutex mtx;
sem_t empty_slots;
sem_t filled_slots; // mutex y semaforos para sincronizar
string log_prod = "";
string log_cons = ""; // Contendrán el historial de acciones


// Se registra un mensaje en el log de consumidores o productores
void regist(string msg, int id_log){
    if (id_log == 0){ // 0 significa que se guarda en los productores
        log_prod += msg + "\n";
    }else{ // Si no, se guarda en el de los consumidores
        log_cons += msg + "\n";
    }
    cout << msg << endl;
}


// Se produce con el formato id_n°producto
string produce(int id, int num_prod){
    string product = to_string(id) + "_" + to_string(num_prod);
    return product;
}


// Funcion para un productor
void producer_func(int num){
    string msg;
    int id = num;
    for (int i=0; i < npp; i++){
        mtx.lock(); // Bloquear para evitar el acceso de otra hebra
        string product = produce(id, i+1);
        msg = to_string(id) + " " + "produjo " + product;
        regist(msg, 0);
        if (buffer.size() < bc){
            // Si no esta lleno el bufer se inserta
            sem_wait(&empty_slots); // Disminuir el semaforo de espacios vacios
            buffer.push(product); // Insertar el producto
            msg = to_string(id) + " " + product + " Inserción exitosa";
            regist(msg, 0); // Notificar y escribir la accion
            sem_post(&filled_slots); // Aumentar el semaforo de espacios llenos
            mtx.unlock(); // Desbloquear
            sleep(rand() % 4 + 1); // Dormir entre 1 y 5 segundos
        }else{
            // Si esta lleno, notificar y esperar hasta que se haga un espacio
            while (1){
                msg = to_string(id) + " " + product + " Buffer lleno - Error de inserción";
                regist(msg, 0); // Notificar y registrar que no se pudo
                mtx.unlock(); // Desbloquear
                sleep(rand() % 4 + 1); // Dormir entre 1 y 5 segundos
                mtx.lock(); // Volver a bloquear para la siguiente iteracion
                // Revisar si se hizo un espacio
                if (buffer.size() < bc){
                    i--; // Hacer que la misma hebra inserte al siguiente ciclo
                    mtx.unlock(); // Desbloquear
                    break; // Romper para volver al ciclo for
                }
            }
        }
    }
}


// Funcion para un consumidor
void consumer_func(int num){
    string msg;
    int id = num;
    for(int i=0; i < ncc; i++){
        // Se disminuye el semaforo de espacios llenos, si esta vacio el buffer
        // se espera bloqueado hasta que haya un espacio
        sem_wait(&filled_slots);
        mtx.lock(); // Previene que otras hebras consuman a la vez
        msg = to_string(id) + " intentando eliminar el elemento del búfer";
        regist(msg, 1); // Notificar y registrar accion
        string consumed = buffer.front(); // Elemento a consumir
        buffer.pop(); // Eliminar el elemento consumido del buffer
        msg = to_string(id) + " " + consumed + " eliminado con éxito";
        regist(msg, 1); // Notificar y registrar accion
        mtx.unlock(); // Desbloquear
        sem_post(&empty_slots); // Aumentar el semaforo de espacios vacios
        sleep(rand() % 4 + 1); // Dormir entre 1 y 5 segundos
    }
}


// Funcion principal
int main(int argc, char *argv[]){

    srand(time(NULL)); // Seed para la generacion de numeros aleatorios

    // Verificar que se ingrese una cantidad valida de datos
    if (argc != 6){
        cout << "Cantidad de datos no valida" << endl;
        return 0;
    }

    // Se guardan los valores en las variables globales
    np = atoi(argv[1]);
    nc = atoi(argv[2]);
    bc = atoi(argv[3]);
    npp = atoi(argv[4]);
    ncc = atoi(argv[5]);

    // Se verifica que los parametros cumplan con las relaciones requeridas
    for (int i=1; i < argc; i++){
        int num = atoi(argv[i]);
        if (num <= 0){ // No se pueden ingresar valores menores o iguales a 0
            cout << "Solo ingresar numeros positivos" << endl;
            return 0;
        }
    }
    // Verificar que el buffer tenga capacidad para almacenar posibles sobrantes
    if (np*npp - nc*ncc > bc){
        cout << "Datos no validos" << endl;
        return 0;
    }

    // Inicializacion de semaforos
    sem_init(&filled_slots, 0, 0);
    sem_init(&empty_slots, 0, bc);

    // Arreglos para almacenar las hebras
    thread consumers[nc];
    thread producers[np];

    // Se crean np productores
    string msg;
    for (int i=0; i < np; i++){
        producers[i] = thread(producer_func, i);
        if (producers[i].joinable()){
            msg = "Productor " + to_string(i) + " creado";
            mtx.lock();
            regist(msg, 0); // Si se crea correctamente se registra y notifica
            mtx.unlock();
        }
    }

    // Se crean nc consumidores
    for (int i=0; i < nc; i++){
        consumers[i] = thread(consumer_func, i);
        if (consumers[i].joinable()){
            msg = "Consumidor " + to_string(i) + " creado";
            mtx.lock();
            regist(msg, 1); // Si se crea correctamente se registra y notifica
            mtx.unlock();
        }
    }

    // Se espera a que terminen los productores
    for (int i=0; i < np; i++){
        producers[i].join();
        if (!producers[i].joinable()){
            msg = "Productor " + to_string(i) + " ha terminado";
            mtx.lock();
            regist(msg, 0); // Si terminan correctamente se registra y notifica
            mtx.unlock();
        }
    }

    // Se espera a que terminen los consumidores
    for (int i=0; i < nc; i++){
        consumers[i].join();
        if (!consumers[i].joinable()){
            msg = "Consumidor " + to_string(i) + " ha terminado";
            mtx.lock();
            regist(msg, 1); // Si terminan correctamente se registra y notifica
            mtx.unlock();
        }
    }

    // Destruccion de semaforos
    sem_destroy(&empty_slots);
    sem_destroy(&filled_slots);

    // Notificar termino del programa y guardar el historial
    cout << "Terminando..." << endl;
    cout << "Quedaron " << to_string(buffer.size()) << " productos en el buffer" << endl;

    // Archivos a usar
    ofstream prod_regist; // Para productores
    ofstream cons_regist; // Para consumidores
    prod_regist.open("productores.txt"); // Si no existen se crean, si ya
    cons_regist.open("consumidores.txt"); // existen se sobrescribe

    // Se escribe en los archivos
    prod_regist << log_prod;
    cons_regist << log_cons;

    // Se cierran los archivos
    prod_regist.close();
    cons_regist.close();

    return 0;
}
